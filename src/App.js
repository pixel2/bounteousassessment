import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      substring: ''
    };
  }

  componentDidMount = () => {

  }

  getTVShowNames = (substr) => {
    substr !== "" ? (

      //Get Shows
      fetch("http://api.tvmaze.com/search/shows?q=" + substr).then(res => res.json())
        .then(
          (result) => {
            let shows = [], sortedShows = [], Output = "", showsWithoutImage =0;
            if (result.length > 0) {
              result.map(res => {
                res.show.image ? showsWithoutImage=showsWithoutImage:showsWithoutImage +=1;
                shows.push(res.show);
              });

              //Sort shows by name
              sortedShows = shows.sort((a, b) => {
                return a.name > b.name ? 1 : -1;
              });
              //Set State to display list/ As we are skipping shows without images, I am letting user know
              //if we have entire list without any shows (Example substring: ff)
              showsWithoutImage !== sortedShows.length ?
               this.setState({ sortedShows: sortedShows, errorMessage: null }) 
               :this.setState({ sortedShows: null, 
                errorMessage: "We have "+showsWithoutImage+" show(s) without image." });
              

              //Formatting output
              sortedShows.map((show, index) => {
                index === sortedShows.length - 1 ? 
                Output += show.name : Output += show.name + " ";
              });

              //Task 1 Output
              console.log(Output);
            }
            else {
              //If no results found, letting user know
              this.setState({ sortedShows: null, errorMessage: "No results found." });
            }

          },
          (error) => {
            //If we get error from API
            this.setState({
              sortedShows: null,
              errorMessage: "Sorry, Please try again later."
            });
          })
    ) : this.setState({ sortedShows: null, errorMessage: "Please enter a show name." });
  }


  render() {

    return (
      <div className="App">
        <div className="search-component">
          <input type="text" className="search-field" placeholder="Enter Show Name"
            onChange={(e) => { this.setState({ substring: e.target.value }) }}
            onKeyDown={(e) => { if (e.key === 'Enter') this.getTVShowNames(this.state.substring) }}
            value={this.state.substring} />
          <button className="btn"
            onClick={() => { this.getTVShowNames(this.state.substring) }} >GET SHOWS</button>
        </div>
        <div className="results-component" >
          {this.state.sortedShows ?
            this.state.sortedShows.map((show, index) => {
              return (
                <React.Fragment key={index}>
                  {show.image ?
                    <div className="show-card">
                      <img 
                      alt={show.name}
                      src={show.image.medium} />
                      <h4>{show.name}</h4>
                    </div> : null}
                </React.Fragment>
              )
            })
            : null
          }

          {this.state.errorMessage ?
            <div>
              {this.state.errorMessage}
            </div> : null
          }
        </div>

      </div>
    );
  }
}

export default App;
